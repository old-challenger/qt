# URL-ссылки, полезные для пыполнения долгосрочного задания

## Текст
* [Завершён перевод книги «Pro Git»](https://habr.com/en/post/150673/).
* [Qt for Developers](https://www.qt.io/developers).
* [Qt Documentation](https://doc.qt.io/).
* [Qt Offline Installers](https://www.qt.io/offline-installers).

## Видео
* [03 - Qt C++ - Создание диалоговых окон](https://www.youtube.com/watch?v=rf_8SV6e3vM).
* [04 - Qt С++ - Обработка событий](https://www.youtube.com/watch?v=hMeEoVLc1ug).
* [05 - Qt C++ - Создание главного окна](https://youtu.be/w42xAH4XMSM).
* [06 - Qt C++ - Технология Model-View](https://youtu.be/GMbSh6-cwD4).
* [07 - Qt C++ - Новая версия. Работа над ошибками](https://youtu.be/w1-_5tvFbCQ).
* [08 - Qt C++ - proxy-model и delegate в Model-View](https://youtu.be/Naiu-0tkA8U).