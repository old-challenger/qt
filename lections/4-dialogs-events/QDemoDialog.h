#ifndef QT_WINDOW_H_MRV
#define QT_WINDOW_H_MRV

#include <QDialog>

class QLineEdit;
class QTextEdit;

class QDemoDialog : public QDialog {
   Q_OBJECT
public:
    QDemoDialog ();

   bool event ( QEvent * event_p ) override;
   
   //bool eventFilter( QObject * watched_p, QEvent * event_p ) override;
protected:

   void keyPressEvent( QKeyEvent * event_p ) override;

   void mousePressEvent( QMouseEvent * event_p ) override;

private:
   QLineEdit * name_m = nullptr;
   QTextEdit * text_m = nullptr;
private slots:
   void operate ();
}; // class QWindow

#endif // QT_MAIN_WINDOW