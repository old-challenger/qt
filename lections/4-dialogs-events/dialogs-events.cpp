#include "QDemoDialog.h"

#include <QApplication>
#include <QDialog>
#include <QLabel>

int main ( int num_p, char ** args_p ) {
   QApplication app_l( num_p, args_p );
   auto * const dialog_l = new QDemoDialog();
   dialog_l->show();
   /*if (QDialog::Accepted == dialog_l->exec())
      mainWnd_l->setText( "Ok" );
   else
      mainWnd_l->setText("Cancel");*/
   return app_l.exec();
}