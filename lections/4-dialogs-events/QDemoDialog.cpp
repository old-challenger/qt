#include "QDemoDialog.h"

#include <QLatin1String>
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QLineEdit> 
#include <QKeyEvent>
#include <QDialog>
#include <QLabel>
#include <QPixmap>

#include <cassert>

QDemoDialog::QDemoDialog () : QDialog( nullptr ) {
   auto  * const vLayout_l = new QVBoxLayout( this );
   auto  * const topLayout_l = new QHBoxLayout(),
         * const buttonsLayout_l = new QHBoxLayout();
   vLayout_l->addLayout( topLayout_l );
      topLayout_l->addWidget( new QLabel( "Name :", this ) );
      topLayout_l->addWidget( name_m = new QLineEdit( this ) );
   vLayout_l->addWidget( text_m = new QTextEdit( this ) );
         text_m->setFrameShadow( QFrame::Plain );
         text_m->setFrameShape( QFrame::Box );
   auto  * const okBtn_l      = new QPushButton( "Ok", this ),
         * const cancelBtn_l  = new QPushButton( "Cancel", this );
   vLayout_l->addLayout( buttonsLayout_l );
      buttonsLayout_l->addStretch( 1 );
      buttonsLayout_l->addWidget( okBtn_l );
      buttonsLayout_l->addWidget( cancelBtn_l );
      okBtn_l->installEventFilter( this );
      cancelBtn_l->installEventFilter( this );
   setWindowTitle( "Test" );
   installEventFilter(this);

   connect( okBtn_l, SIGNAL( released() ), this, SLOT( operate() ) );
   connect( cancelBtn_l, SIGNAL( released() ), this, SLOT( reject() ) );
}

bool QDemoDialog::event( QEvent * event_p ) {
   if ( QEvent::MouseButtonDblClick == event_p->type() ) {
      auto * const mouseEvent_l = dynamic_cast< QMouseEvent * >( event_p );
      assert( nullptr != mouseEvent_l );
      text_m->setText(
            QLatin1String( "globalX: " )  + QString::number( mouseEvent_l->globalX() ) + QLatin1String( "\n" ) +
            QLatin1String( "globalY: " )  + QString::number( mouseEvent_l->globalY() ) + QLatin1String( "\n" ) +
            QLatin1String( "X: " )        + QString::number( mouseEvent_l->x() )       + QLatin1String( "\n" ) +
            QLatin1String( "Y: " )        + QString::number( mouseEvent_l->y() )       + QLatin1String( "\n" )
         );
      return true;
   }
   return QDialog::event( event_p );
}

//bool QWindow::eventFilter( QObject * watched_p, QEvent * event_p ) {
//   if ( QEvent::MouseButtonDblClick == event_p->type() ||
//        QEvent::MouseButtonPress == event_p->type() )
//        return true;
//   return false;
//}

void QDemoDialog::keyPressEvent( QKeyEvent * event_p ) {
   if ( Qt::Key_Q == event_p->key() && ( Qt::ControlModifier & event_p->modifiers() ) )
      reject();
}

void QDemoDialog::mousePressEvent( QMouseEvent * event_p ) {
   setWindowTitle( windowTitle() + "+" );
   event_p->accept();
}

void QDemoDialog::operate() {
   name_m->setText("");
   text_m->setText("");
   accept();
}

