#include <QAddDialog.h>
#include <pics.h>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTextCodec>
#include <QTextEdit>
#include <QLineEdit> 
#include <QKeyEvent>
#include <QMouseEvent>
#include <QDialog>
#include <QLabel>
#include <QPixmap>

QAddDialog::QAddDialog() : QDialog(nullptr), name_m(nullptr), text_m( nullptr ) {
   QVBoxLayout * const vLayout_l = new QVBoxLayout( this );
   QHBoxLayout * const topLayout_l = new QHBoxLayout(),
               * const buttonsLayout_l = new QHBoxLayout();
   vLayout_l->addLayout( topLayout_l );
      topLayout_l->addWidget( new QLabel( tr( "Name :" ), this ) );
      topLayout_l->addWidget( name_m = new QLineEdit( this ) );
   vLayout_l->addWidget( text_m = new QTextEdit( this ) );
         text_m->setFrameShadow( QFrame::Plain );
         text_m->setFrameShape( QFrame::Box );
   QPushButton * const okBtn_l = new QPushButton( utl::icon::createPixmap( pics::check24() ), tr("Ok"), this),
               * const cancelBtn_l = new QPushButton( utl::icon::createPixmap( pics::cross24() ), tr("Cancel"), this );
   vLayout_l->addLayout( buttonsLayout_l );
      buttonsLayout_l->addStretch( 1 );
      buttonsLayout_l->addWidget( okBtn_l );
      buttonsLayout_l->addWidget( cancelBtn_l );
   setWindowTitle( tr( "Test" ) );

   connect( okBtn_l, SIGNAL(released()), this, SLOT(accept()));
   connect( cancelBtn_l, SIGNAL( released() ), this, SLOT( reject() ) );
}

QPair<QString, QString> QAddDialog::textPairValues() const {
   return QPair<QString, QString>(name_m->text(), text_m->toPlainText());
}

void QAddDialog::keyPressEvent(QKeyEvent * event_p) {
   if (Qt::Key_Q == event_p->key() && (Qt::ControlModifier & event_p->modifiers()))
      reject();
}

void QAddDialog::mousePressEvent(QMouseEvent * event_p) {
   setWindowTitle( windowTitle() + "+" );
   event_p->accept();
}