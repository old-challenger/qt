#include <QExampleItemModel.h>
#include "pics.h"

#include <QSet>
#include <list>
#include <cassert>
#include <algorithm>

QExampleItemModel::QExampleItemModel( QObject * parent_p = 0 ) : QAbstractItemModel( parent_p ) { 
   initTestData(); 
}

QVariant QExampleItemModel::headerData( int section_p, Qt::Orientation orientation_p, int role_p = Qt::DisplayRole ) const {
   if ( Qt::Orientation::Horizontal != orientation_p || Qt::DisplayRole != role_p )
      return QVariant();
   return section_p ? tr( "Text" ) : tr( "Name" );
}

QVariant QExampleItemModel::data( const QModelIndex & index_p, int role_p = Qt::DisplayRole ) const {
   if ( !index_p.isValid() )
      return QVariant();
   const auto & element_cl = constElementByPointer( index_p.internalId() );
   switch ( role_p) {
   case Qt::DisplayRole: switch ( index_p.column() ) {
      case 0:
         return element_cl.name();
      case 1:
         return element_cl.text();
      default:
         assert( !"[ERROR] Incorrect switch branch." );
         return QVariant();
      }
   case Qt::DecorationRole: { 
      static const auto plus_scl = utl::icon::createPixmap( pics::plus24() ),
                        cross_scl = utl::icon::createPixmap( pics::cross24() );
      if ( index_p.column() )
         return QVariant();
      else if ( element_cl.name().length() > 1 )
         return plus_scl;
      else
         return cross_scl;
   }
   case Qt::TextAlignmentRole: switch ( index_p.column() ) {
      case 0:
         return Qt::AlignCenter;
      default:
         return Qt::AlignRight;
   }
   default:
      return QVariant();
   } // switch ( role_p) {
}

QModelIndex QExampleItemModel::index( int row_p, int column_p, const QModelIndex & parent_p = QModelIndex() ) const {
   const auto & index_cl = parent_p.isValid() ? constElementByPointer( parent_p.internalId() ).constChilds() : root_m;
   return index_cl.size() > row_p ? createIndex( row_p, column_p, index_cl.at( row_p ) ) : QModelIndex();
}

QModelIndex QExampleItemModel::parent( const QModelIndex & index_p ) const {
   const int key_cl = constElementByPointer( index_p.internalId() ).parent();
   return modelElement::INCORRECT_ID == key_cl 
      ? QModelIndex() : createIndex( data_m.at( key_cl ).localId(), 0, key_cl );
}

int QExampleItemModel::rowCount( const QModelIndex & parent_cp = QModelIndex() ) const {
   return static_cast< int >( parent_cp.isValid() 
      ? constElementByPointer( parent_cp.internalId() ).constChilds().size() : root_m.size() );
}

bool QExampleItemModel::setData( const QModelIndex & index_cp, const QVariant & value_cp, int role_p/* = Qt::EditRole*/ ) {
   if ( !index_cp.isValid() )
      return false;
   switch ( role_p ) {
   case Qt::EditRole: { 
      auto & element_l = elementByPointer( index_cp.internalId() );   
      switch ( index_cp.column() ) {
      case 0:
         element_l.setName( value_cp.toString() );
         emit dataChanged( index_cp, index_cp, { Qt::EditRole } );
         return true; 
      case 1:
         element_l.setText( value_cp.toString() );
         emit dataChanged( index_cp, index_cp, { Qt::EditRole } );
         return true; 
      default:
         assert( !"[ERROR] Incorrect switch branch." );
         return false;
      } }
   default:
      assert( !"[ERROR] Incorrect switch branch." );
      return false;
   } // switch ( role_p) {
}

void QExampleItemModel::addRow( const QPair< QString, QString > & pair_p,
   int parent_p = modelElement::INCORRECT_ID, int localId_p = modelElement::INCORRECT_ID ) {
   beginResetModel();
   const int   parentKey_cl = modelElement::INCORRECT_ID == localId_p
                  ? modelElement::INCORRECT_ID : modelElement::INCORRECT_ID == parent_p
                     ? root_m.at( localId_p ) : data_m.at( parent_p ).constChilds().at( localId_p ),
               key_cl = data_m.size() ? data_m.rbegin()->first + 1 : 0;
   data_m.insert( data_m.cend(), 
      std::make_pair( key_cl, modelElement( pair_p.first, pair_p.second, parentKey_cl ) ) );
   rebuildIndex( parentKey_cl, { key_cl } );
   endResetModel();
}

void QExampleItemModel::removeRowsFromData( const QModelIndexList & list_p ) {
   beginResetModel();

   QSet< int > keysForRebuild_l;
   for ( auto index : list_p ) {
      const int key_cl = int( index.internalId() );
      const auto found_cl = data_m.find( key_cl );
      if ( data_m.end() == found_cl )
         continue;
      keysForRebuild_l.insert( found_cl->second.parent() );
      std::list< int > eraseQueue_l;
      eraseQueue_l.push_back( key_cl );
      while ( eraseQueue_l.size() ) {
         const auto foundElementFromQueue_cl = data_m.find( eraseQueue_l.front() );
         if ( data_m.end() != foundElementFromQueue_cl ) {
            eraseQueue_l.insert( eraseQueue_l.end(), 
               foundElementFromQueue_cl->second.constChilds().begin(),
               foundElementFromQueue_cl->second.constChilds().end() );
            data_m.erase( eraseQueue_l.front() );
         }
         eraseQueue_l.pop_front();
      }
   }

   for ( auto id : keysForRebuild_l )
      rebuildIndex( id );
   endResetModel();
}

void QExampleItemModel::addId2index( int id_p, modelElement::index_type & index_rp ) {
   auto found_l = data_m.find( id_p );
   if ( data_m.end() != found_l ) {
      found_l->second.setLocalId( static_cast< int >( index_rp.size() ) );
      index_rp.push_back( id_p );
   }
}

void QExampleItemModel::initTestData() {
   static const auto zero_scl    = QString::number( 0 ),
                     one_scl     = QString::number( 1 ),
                     two_scl     = QString::number( 2 ),
                     test_scl    = static_cast< QString >( QLatin1String( "TEST" ) );

   data_m = {
      { 0, modelElement{ zero_scl, test_scl, modelElement::INCORRECT_ID } },
      { 1, modelElement{ one_scl, test_scl, modelElement::INCORRECT_ID, { 3, 4, 5 } } },
         { 3, modelElement{ one_scl + zero_scl, test_scl, 1 } },
         { 4, modelElement{ one_scl + one_scl, test_scl, 1 } },
         { 5, modelElement{ one_scl + two_scl, test_scl, 1 } },
      { 2, modelElement{ two_scl, test_scl, modelElement::INCORRECT_ID, { 6, 7, 8 } } },
         { 6, modelElement{ two_scl + zero_scl, test_scl, 2, { 9, 10, 11 } } },
            { 9,  modelElement{ two_scl + zero_scl + zero_scl, test_scl, 6 } },
            { 10, modelElement{ two_scl + zero_scl + one_scl, test_scl, 6 } },
            { 11, modelElement{ two_scl + zero_scl + two_scl, test_scl, 6 } },
         { 7, modelElement{ two_scl + one_scl, test_scl, 2, { 12, 13, 14 } } },
            { 12, modelElement{ two_scl + one_scl + zero_scl, test_scl, 7 } },
            { 13, modelElement{ two_scl + one_scl + one_scl, test_scl, 7 } },
            { 14, modelElement{ two_scl + one_scl + two_scl, test_scl, 7 } },
         { 8, modelElement{ two_scl + two_scl, test_scl, 2, { 15, 16, 17 } } },
            { 15, modelElement{ two_scl + two_scl + zero_scl, test_scl, 8 } },
            { 16, modelElement{ two_scl + two_scl + one_scl, test_scl, 8 } },
            { 17, modelElement{ two_scl + two_scl + two_scl, test_scl, 8 } }
   };

   rebuildIndex( modelElement::INCORRECT_ID, { 0, 1, 2 } );
   rebuildIndex( 1 );
   rebuildIndex( 2 );
   rebuildIndex( 6 );
   rebuildIndex( 7 );
   rebuildIndex( 8 );
}

void QExampleItemModel::rebuildIndex( int parentKey_p, const modelElement::index_type & newIds_p ) {
   auto & targetIndex_rl = modelElement::INCORRECT_ID == parentKey_p ? root_m : data_m[ parentKey_p ].childs();
   modelElement::index_type newIndex_l;
   addIds2index( targetIndex_rl, newIndex_l );
   addIds2index( newIds_p, newIndex_l );
   targetIndex_rl = std::move( newIndex_l );
}