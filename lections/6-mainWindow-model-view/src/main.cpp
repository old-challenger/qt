#include <QMainWidget.h>
#include <QApplication>
//#include <QTranslator>
//#include <QMessageBox>

int main ( int num_p, char ** args_p ) {
   QApplication app_l( num_p, args_p );
   
   //QTranslator translator_l;
   //if ( !translator_l.load( "russian.qm" ) )
   //   QMessageBox::warning( nullptr, "Translation warning", "Can't load file russian.qm" );
   //if ( !app_l.installTranslator( &translator_l ) )
   //   QMessageBox::warning( nullptr, "Translation warning", "Can't install translator" );
   
   auto * const mainWnd_l = new QMainWidget{};
   mainWnd_l->show();
   return app_l.exec();
}