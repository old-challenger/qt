#include <QExampleItemModel.h>
#include <QExampleDelegate.h>
#include <QMainWidget.h>
#include <QAddDialog.h>
#include <pics.h>

#include <QSortFilterProxyModel>
#include <QItemSelectionModel>
#include <QStyledItemDelegate>
#include <QMessageBox>
#include <QHeaderView>
#include <QTableView>
#include <QStatusBar>
#include <QTextCodec>
#include <QListView>
#include <QTreeView>
#include <QMdiArea>
#include <QMenuBar>
#include <QToolBar>
#include <QLabel>

#include <cassert>

QMainWidget::QMainWidget() : QMainWindow(), addAction_m( nullptr ), removeAction_m( nullptr ), codecsAction_m( nullptr ),
   addDialog_m( nullptr ), model_m( nullptr ), sortingModel_m( nullptr ), selectionModel_m( nullptr ) {
   // ������� ����
   auto  * const tableMenu_l  = menuBar()->addMenu( tr( "&Table" ) ),
         * const helpMenu_l   = menuBar()->addMenu( tr( "&Help" ) );
   // ���������� ������ ���� "table"
   addAction_m    = tableMenu_l->addAction( utl::icon::createPixmap( pics::plus24() ),    tr( "&Add" ) );
   removeAction_m = tableMenu_l->addAction( utl::icon::createPixmap( pics::cross24() ),   tr( "&Remove" ) );
   tableMenu_l->addSeparator();
   auto * const exitAction_l = tableMenu_l->addAction( tr( "E&xit" ) );
      exitAction_l->setShortcut( QKeySequence( Qt::CTRL + Qt::Key_Q ) );
   
   // ���������� ������ ���� "help"
   codecsAction_m = helpMenu_l->addAction( tr( "Available Codecs" ) );
   helpMenu_l->addAction( tr("About") );

   // ������ ������������
   auto * const toolBar_l = addToolBar( "single" );
      toolBar_l->addAction( addAction_m );
      toolBar_l->addAction( removeAction_m );

   connect( exitAction_l,     SIGNAL( triggered() ), this, SLOT( close()        ) );
   connect( addAction_m,      SIGNAL( triggered() ), this, SLOT( slotReAction() ) );
   connect( removeAction_m,   SIGNAL( triggered() ), this, SLOT( slotReAction() ) );
   connect( codecsAction_m,   SIGNAL( triggered() ), this, SLOT( slotReAction() ) );

   // ����������� ������
   auto * const mdiArea_l = new QMdiArea( this );
      // ������ ������
      model_m = new QExampleItemModel( this );
      sortingModel_m = new QSortFilterProxyModel( this );
      sortingModel_m->setSourceModel( model_m );
      selectionModel_m = new QItemSelectionModel( sortingModel_m, this );
      auto * const modelsDelegate_l = new QExampleDelegate( this );

      auto * const tree_l = new QTreeView( mdiArea_l );
      tree_l->setSortingEnabled( true );
      mdiArea_l->addSubWindow( configViewWidget(   tree_l, tr("tree"), sortingModel_m, 
                                                   selectionModel_m, modelsDelegate_l ) );

      auto * const table_l = new QTableView( mdiArea_l );
      table_l->setSortingEnabled( true );
      mdiArea_l->addSubWindow( configViewWidget(   table_l, tr( "table"), sortingModel_m, 
                                                   selectionModel_m, modelsDelegate_l ) );
         
      auto * const list_l = new QListView( mdiArea_l );
      mdiArea_l->addSubWindow( configViewWidget(   list_l, tr( "list" ), sortingModel_m, 
                                                   selectionModel_m, modelsDelegate_l ) );
   setCentralWidget( mdiArea_l );

   // ������ ���������
   statusBar()->insertWidget( 0, new QLabel( statusBar() ), 1 );
   statusBar()->insertWidget( 1, new QLabel( tr( "status bar widget" ), statusBar() ) );
   statusBar()->showMessage( tr( "Application started" ), 5000 );

   addDialog_m = new QAddDialog();
}

QMainWidget::~QMainWidget(){
   addDialog_m->deleteLater();
}

void QMainWidget::addRowDialog( const QPair< int, int > & isPair_p ) {
   if ( QDialog::Accepted == addDialog_m->exec() ) {
      model_m->addRow( addDialog_m->textPairValues(), isPair_p.first, isPair_p.second );
      statusBar()->showMessage( tr( "Data element was added" ), 3000 );
   }
}

QModelIndexList QMainWidget::transformIndexes( const QModelIndexList & indexes_cp ) {
   QModelIndexList result_l;
   for ( auto index : indexes_cp )
      result_l.push_back( sortingModel_m->mapToSource( index ) );
   return std::move( result_l );
}

void QMainWidget::slotReAction() {
   auto const * const sender_cl = dynamic_cast< QAction * >( sender() );
   assert( sender_cl );
   if ( sender_cl == addAction_m ) { 
      if ( selectionModel_m->hasSelection() ) {
         QList< QPair< int, int > > parents_l;
         for ( auto index : transformIndexes( selectionModel_m->selectedRows() ) ) {
            auto & element_l = model_m->constElementByPointer( index.internalId() );
            parents_l.push_back( QPair< int, int >( element_l.parent(), element_l.localId() ) );
         }
         for ( auto pair : parents_l )
            addRowDialog( pair );
      } else
         addRowDialog( QPair< int, int >(   QExampleItemModel::modelElement::INCORRECT_ID, 
                                            QExampleItemModel::modelElement::INCORRECT_ID ) );
   } else if ( sender_cl == removeAction_m ) {
      if ( selectionModel_m->hasSelection() )
         model_m->removeRowsFromData( transformIndexes( selectionModel_m->selectedRows() ) );
   } else if ( sender_cl == codecsAction_m ) {
      static const auto availableCodecs_scl = QTextCodec::availableCodecs();
      QString codecsStr_l;
      for (auto codec : availableCodecs_scl)
         codecsStr_l += QString(codec) + ", ";
      codecsStr_l.remove( codecsStr_l.size() - 2, 2 );
      codecsStr_l += ".";
      QMessageBox::information( nullptr, tr( "Available Codecs" ), codecsStr_l );
   } else
      assert( !"[ERROR] Incorrect if branch." );
}