#include <QExampleDelegate.h>
#include <QExampleItemModel.h>

#include <regex>
#include <string>

QWidget * QExampleDelegate::createEditor( QWidget * parent_p, const QStyleOptionViewItem & /*option_cp*/,
   const QModelIndex & index_cp ) const {
   static const std::regex digitsRegEx_scl( "\\d+" );
   assert( index_cp.isValid() );
   //if ( std::regex_match( modelData( index_cp ).toStdString(), digitsRegEx_scl ) ) 
   if ( std::regex_match( index_cp.data().toString().toStdString(), digitsRegEx_scl ) ) 
      return new QSpinBox( parent_p );
   else
      return new QLineEdit( parent_p );
}

void QExampleDelegate::setEditorData( QWidget * editor_p, const QModelIndex & index_cp ) const {
   assert( index_cp.isValid() );
   //forWidget( editor_p ).setData( modelData( index_cp ) );
   static const std::string   spinBoxClassName_scl = "QSpinBox",
                              lineEditClassName_scl = "QLineEdit";
   const auto * const metaObj_l = editor_p->metaObject();
   if ( lineEditClassName_scl == metaObj_l->className() ) {
      auto * const lineEditWidget_l = dynamic_cast< QLineEdit * >( editor_p );
      assert( nullptr != lineEditWidget_l );
      lineEditWidget_l->setText( index_cp.data().toString() );
   } else if ( spinBoxClassName_scl == metaObj_l->className() ) {
      auto * const spinBoxWidget_l = dynamic_cast< QSpinBox * >( editor_p );
      assert( nullptr != spinBoxWidget_l );
      spinBoxWidget_l->setValue( index_cp.data().toInt() );
   } else 
      assert( !"[ERROR] Incorrect widget class as delegate using" );
}

void QExampleDelegate::setModelData( QWidget * editor_p, QAbstractItemModel * model_p,
   const QModelIndex & index_cp ) const {
   assert( index_cp.isValid() );
   //model_p->setData( index_cp, forWidget( editor_p ).getData(), Qt::EditRole );
   static const std::string   spinBoxClassName_scl = "QSpinBox",
                              lineEditClassName_scl = "QLineEdit";
   const auto * const metaObj_l = editor_p->metaObject();
   if ( lineEditClassName_scl == metaObj_l->className() ) {
      auto * const lineEditWidget_l = dynamic_cast< QLineEdit * >( editor_p );
      assert( nullptr != lineEditWidget_l );
      model_p->setData( index_cp, lineEditWidget_l->text(), Qt::EditRole );
   } else if ( spinBoxClassName_scl == metaObj_l->className() ) {
      auto * const spinBoxWidget_l = dynamic_cast< QSpinBox * >( editor_p );
      assert( nullptr != spinBoxWidget_l );
      model_p->setData( index_cp, spinBoxWidget_l->value(), Qt::EditRole );
   } else 
      assert( !"[ERROR] Incorrect widget class as delegate using" );
}

void QExampleDelegate::updateEditorGeometry( QWidget * editor_p, const QStyleOptionViewItem & option_cp,
   const QModelIndex & /*index_cp*/ ) const {
   editor_p->setGeometry( option_cp.rect );
}

//const QString & QExampleDelegate::modelData ( const QModelIndex & index_cp ) const {
//   const auto& currentElement_cl = model_m->constElementByPointer(index_cp.internalId());
//   return index_cp.column() ? currentElement_cl.text() : currentElement_cl.name();
//}