<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>QAddDialog</name>
    <message>
        <location filename="QAddDialog.cpp" line="21"/>
        <source>Name :</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="QAddDialog.cpp" line="26"/>
        <source>Ok</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="QAddDialog.cpp" line="27"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="QAddDialog.cpp" line="32"/>
        <source>Test</source>
        <translation>Тест</translation>
    </message>
</context>
<context>
    <name>QMainWidget</name>
    <message>
        <location filename="QMainWidget.cpp" line="18"/>
        <source>&amp;Table</source>
        <translation>&amp;Таблица</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="19"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="21"/>
        <source>&amp;Add</source>
        <translation>&amp;Добавить</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="22"/>
        <source>&amp;Remove</source>
        <translation>&amp;Удалить</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="24"/>
        <source>E&amp;xit</source>
        <translation>&amp;Выход</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="26"/>
        <location filename="QMainWidget.cpp" line="99"/>
        <source>Available Codecs</source>
        <translation>Доступные кодеки</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="27"/>
        <source>About</source>
        <translation>Что это?</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="42"/>
        <source>tree</source>
        <translation>дерево</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="50"/>
        <source>table</source>
        <translation>таблица</translation>
    </message>
    <message>
        <location filename="QMainWidget.cpp" line="58"/>
        <source>list</source>
        <translation>список</translation>
    </message>
</context>
</TS>
