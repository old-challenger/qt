#ifndef QT_MAIN_WIDGET_H_MRV
#define QT_MAIN_WIDGET_H_MRV

#include <QMainWindow>
#include <QModelIndex>
#include <QAbstractItemView>

class QAddDialog;
class QExampleItemModel;
class QItemSelectionModel;
class QSortFilterProxyModel;

//template< class data_type >
//class QTestTemplate : public QMainWindow {
//Q_OBJECT
//   data_type data_m;
//};

class QMainWidget : public QMainWindow {
Q_OBJECT
public:
   
   QMainWidget();
   
   ~QMainWidget();
private:
   QAction * addAction_m, * removeAction_m, * codecsAction_m;
   QAddDialog * addDialog_m;

   QExampleItemModel       * model_m;
   QSortFilterProxyModel   * sortingModel_m;
   QItemSelectionModel     * selectionModel_m;

   template< class mainModel_type, class selectionModel_type, class delegate_type >
   static QAbstractItemView * configViewWidget( QAbstractItemView * view_p, const QString& tytle_cp,
                                                mainModel_type * model_p, selectionModel_type * selectionModel_m,
                                                delegate_type * delegate_p ) {
      view_p->setWindowTitle( tytle_cp );
      view_p->setSelectionBehavior( QAbstractItemView::SelectRows );
      view_p->setSelectionMode( QAbstractItemView::ExtendedSelection );
      view_p->setModel( model_p );
      view_p->setSelectionModel( selectionModel_m );
      view_p->setItemDelegate( delegate_p );
      return view_p;
   }
   
   void addRowDialog( const QPair< int, int > & isPair_p );

   QModelIndexList transformIndexes ( const QModelIndexList & indexes_cp );
private slots:

   void slotReAction();
};
#endif // QT_MAIN_WIDGET_H_MRV