#ifndef QT_ADD_DIALOG_H_MRV
#define QT_ADD_DIALOG_H_MRV

#include <QDialog>

class QLineEdit;
class QTextEdit;

class QAddDialog : public QDialog {
   Q_OBJECT
public:

   QAddDialog ();

   QPair< QString, QString > textPairValues () const;
protected:

   void keyPressEvent ( QKeyEvent * event_p ) override;

   void mousePressEvent ( QMouseEvent * event_p ) override;

private:
   QLineEdit * name_m;
   QTextEdit * text_m;
};

#endif // #define QT_ADD_DIALOG_H_MRV
