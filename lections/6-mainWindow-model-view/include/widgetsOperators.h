   class forWidget {
   public:
      using input_type = typename const QString;
      using output_type = typename QVariant;

      forWidget ( QWidget * widget_cp ) : widget_m( widget_cp ) { ; }

      decltype( auto ) getData () const {
         output_type result_l;
         smartOperator< getFrom, output_type >( result_l );
         return std::move( result_l );
      }

      void setData ( input_type & data_p ) {
         smartOperator< setTo, input_type >( data_p );
      }
   private:
      QWidget * widget_m;

      template< class object_type, class base_type >
      class operatorBase {
      public:
         operatorBase ( base_type * object_p ) : object_m( dynamic_cast< object_type * >( object_p ) ) {
            assert( nullptr != object_m );
         }
      private:
         object_type * object_m;
      };

      template< class object_type > 
      class getFrom : operatorBase< object_type, QWidget > {
      public:

         getFrom ( QWidget * object_p ) : operatorBase< object_type, QWidget >( object_p ) { ; }
      
         void data( output_type & data_p ) const { operate< object_type >( data_p ); }
      private:
         object_type * object_m;

         template< class object_type > void operate ( output_type & data_p ) const;

         template<> void operate< QLineEdit >( output_type & data_p ) const { data_p = object_m->text(); }
         template<> void operate< QSpinBox >( output_type & data_p ) const { data_p = object_m->value(); }
      };
      
      template< class object_type >
      class setTo : operatorBase< object_type, QWidget > {
      public:

         setTo( QWidget * object_p ) : operatorBase< object_type, QWidget >( object_p ) { ; }

         void data( input_type & data_p ) { operate< object_type >( data_p ); }
      private:
         object_type * object_m;

         template< class object_type > void operate( input_type & data_p );

         template<> void operate< QLineEdit >( input_type & data_p ) { object_m->setText( data_p ); }
         template<> void operate< QSpinBox >( input_type & data_p ) { object_m->setValue( data_p.toInt() ); }
      };

      template< template < class > class operate_template, typename data_type >
      void smartOperator ( data_type & data_p ) const {
         static const std::string   spinBoxClassName_scl = "QSpinBox",
                                    lineEditClassName_scl = "QLineEdit";
         const auto * const metaObj_l = widget_m->metaObject();
         if ( lineEditClassName_scl == metaObj_l->className() ) 
            operate_template< QLineEdit >( widget_m ).data( data_p );
         else if ( spinBoxClassName_scl == metaObj_l->className() )
            operate_template< QLineEdit >( widget_m ).data( data_p );
         else
            assert(!"[ERROR] Incorrect widget class as delegate using");
      }
   };
