#ifndef QT_EXAMPLE_ITEM_MODEL_H_MRV
#define QT_EXAMPLE_ITEM_MODEL_H_MRV

#include <QAbstractItemModel>
#include <QVector>
#include <vector>
#include <map>

class QExampleItemModel : public QAbstractItemModel {
public:   
   class modelElement {
   public:
      using index_type = QVector< int >; // std::vector< int >;
      enum consts : int { INCORRECT_ID = -1 };

      modelElement () = default;

      modelElement ( QString name_p, QString text_p, int parent_p, const index_type & childs_cp = { } ) :
         name_m( name_p ), text_m( text_p ), parent_m( parent_p ), localId_m( INCORRECT_ID ), childs_m( childs_cp ) { ; }

      QString  name     () const { return name_m; }
      QString  text     () const { return text_m; }
      int      parent   () const { return parent_m; }
      int      localId  () const { return localId_m; }
      
      const index_type & constChilds () const { return childs_m; }
      index_type & childs () { return childs_m; }
      
      void setLocalId ( int id_p ) { localId_m = id_p; }
      void setName ( const QString & name_p ) { name_m = name_p; }
      void setText ( const QString & text_p ) { text_m = text_p; }
   private:
      QString name_m, text_m;
      int parent_m = INCORRECT_ID, localId_m = INCORRECT_ID;
      index_type childs_m;
   }; // class modelElement {

   QExampleItemModel( QObject * parent_p );

   inline int columnCount ( const QModelIndex & /*parent_p*/ ) const final { return 2; }

   QVariant headerData ( int section_p, Qt::Orientation orientation_p, int role_p ) const final;

   QVariant data ( const QModelIndex & index_p, int role_p ) const final;

   QModelIndex index ( int row_p, int column_p, const QModelIndex & parent_p ) const final;

   Qt::ItemFlags flags( const QModelIndex & ) const final {
      return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable ;
   }

   QModelIndex parent ( const QModelIndex & index_cp ) const final;

   int rowCount ( const QModelIndex & parent_cp ) const final;

   bool setData ( const QModelIndex & index_cp, const QVariant & value_cp, int role_p = Qt::EditRole ) final;
      
   void addRow ( const QPair< QString, QString > & pair_p, int parent_p, int localId_p );

   const modelElement & constElementByPointer( qintptr pointer_p ) const {
      return data_m.at( static_cast< int >( pointer_p ) );
   }

   void removeRowsFromData( const QModelIndexList & index_p );
private:
   using data_type = std::map< int, modelElement >;

   data_type data_m;
   modelElement::index_type root_m;

   void addId2index ( int id_p, modelElement::index_type & index_rp );

   void addIds2index ( const modelElement::index_type & ids_cp, modelElement::index_type & index_rp ) {
      std::for_each( ids_cp.cbegin(), ids_cp.cend(),
         [ &index_rp, this ]( auto i ) { this->addId2index( i, index_rp ); } );
   }

   modelElement & elementByPointer( qintptr pointer_p ) {
      return const_cast< modelElement & >( constElementByPointer( pointer_p ) );
   }

   void initTestData ();

   void rebuildIndex( int parentKey_p, const modelElement::index_type & newIds_cp = { } );
}; // class QExampleItemModel : public QAbstractItemModel {
#endif // QT_EXAMPLE_ITEM_MODEL_H_MRV