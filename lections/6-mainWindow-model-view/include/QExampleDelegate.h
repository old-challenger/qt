#ifndef QT_EXAMPLE_DELEGATE_H_MRV
#define QT_EXAMPLE_DELEGATE_H_MRV

#include <QStyledItemDelegate>
#include <QLineEdit>
#include <QSpinBox>
#include <QVariant>

#include <algorithm>
#include <cassert>

class QExampleItemModel;

class QExampleDelegate : public QStyledItemDelegate {
public:
   QExampleDelegate ( QObject * parent_p/*, QExampleItemModel * model_p*/ ) : 
      QStyledItemDelegate( parent_p )/*, model_m( model_p )*/ { ; }

   QWidget * createEditor ( QWidget * parent_p, const QStyleOptionViewItem & /*option_cp*/,
      const QModelIndex & index_cp ) const final;

   void setEditorData ( QWidget * editor_p, const QModelIndex & index_cp ) const final;

   void setModelData ( QWidget * editor_p, QAbstractItemModel * model_p,
      const QModelIndex & index_cp ) const final;

   void updateEditorGeometry ( QWidget * editor_p, const QStyleOptionViewItem & option_cp,
      const QModelIndex & /*index_cp*/ ) const final;

   //const QString & modelData ( const QModelIndex & index_cp ) const;
private:
   //QExampleItemModel * const model_m;
}; // class QExampleDelegate : public QStyledItemDelegate {

#endif //QT_EXAMPLE_DELEGATE_H_MRV